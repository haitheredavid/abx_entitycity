﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;


namespace myScript.TestField.ScaleTest {
    public class ScaleSystem_Sys : JobComponentSystem {

        private EntityQuery _group;

        protected override void OnCreate() {
            _group = GetEntityQuery(typeof(Scale), ComponentType.ReadOnly<ScaleSpeed_Comp>());
        }


        [BurstCompile]
        struct ScaleSpeedJob : IJobForEach<Scale, ScaleSpeed_Comp> {

            public float time;

            public void Execute(ref Scale scale, [ReadOnly] ref ScaleSpeed_Comp scaleSpeed) {
                scale.Value = math.sin(time * scaleSpeed.ScalePerSecond);
            }

        }


        [BurstCompile]
        struct ScaleChunkSpeedJob : IJobChunk {

            public float time;
            public ArchetypeChunkComponentType<Scale> ScaleType;
            [ReadOnly] public ArchetypeChunkComponentType<ScaleSpeed_Comp> ScaleSpeedType;

            public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {
                var scaleChunk = chunk.GetNativeArray(ScaleType);
                var scaleSpeedChunk = chunk.GetNativeArray(ScaleSpeedType);

                for (int i = 0; i < chunk.Count; i++) {
                    var scale = scaleChunk[i];
                    var scaleSpeed = scaleSpeedChunk[i];

                    scaleChunk[i] = new Scale {Value = math.sin(time * scaleSpeed.ScalePerSecond)};
                }
            }

        }


        protected override JobHandle OnUpdate(JobHandle inputDeps) {

            var scaleType = GetArchetypeChunkComponentType<Scale>();
            var scaleSpeedType = GetArchetypeChunkComponentType<ScaleSpeed_Comp>(true);

            var job = new ScaleChunkSpeedJob {
                ScaleType = scaleType,
                ScaleSpeedType = scaleSpeedType,
                time = Time.time
            };
//            var job = new ScaleSpeedJob {
//                time = Time.time
//            };

            return job.Schedule(_group, inputDeps);
        }

    }
}