﻿using System;
using Unity.Entities;

namespace myScript.TestField.ScaleTest{
    [Serializable]
    public struct ScaleSpeed_Comp : IComponentData {

        public float ScalePerSecond;

    }
}