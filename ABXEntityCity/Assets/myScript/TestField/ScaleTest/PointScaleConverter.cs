﻿using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace myScript.TestField.ScaleTest {
    [RequiresEntityConversion]
    public class PointScaleConverter : MonoBehaviour, IConvertGameObjectToEntity {

        public float ScaleSpeed = 2.0f;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
            var data = new ScaleSpeed_Comp { ScalePerSecond = ScaleSpeed};
            float scale = gameObject.transform.localScale.x;
            dstManager.AddComponentData(entity, new Scale { Value = scale});
            dstManager.AddComponentData(entity, data);
        }

    }
}