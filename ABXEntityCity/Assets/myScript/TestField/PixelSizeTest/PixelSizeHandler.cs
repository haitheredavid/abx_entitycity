﻿using System;
using System.Collections.Generic;
using GoogleARCore;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using myScript.TestField.VisualizeImageTest;

namespace myScript.TestField.PixelSizeTest {
    public class PixelSizeHandler : MonoBehaviour {

        public Mesh mesh;
        public Material material;
        public Texture2D activeImage;
        public AugmentedImageDatabase database;

        private static EntityManager _entityManager;
        private static EntityArchetype _tileType;

        private ImageReader _imageReader;
        private float3[] _imageValues;

        private bool _created = false;
        private GameObject instance;

        private int _dataIndex;
        private float _width;
        private float _height;
        private List<AugmentedImage> _augmentedImages = new List<AugmentedImage>();

        private void CreateTypes()
            {
                _tileType = _entityManager.CreateArchetype(
                    typeof(Tile),
                    typeof(SetTileData),
                    typeof(Translation)
                );
            }

        private GameObject CreateImage(Texture2D texture, float xSize, float zSize)
            {
                GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Plane);

                plane.transform.localScale = new Vector3(xSize / 2f, 1f, zSize / 2f);
                plane.GetComponent<MeshRenderer>().material.mainTexture = texture;
                return plane;
            }

        private void Awake()
            {
                if (database != null) {
                    for (int i = 0; i < database.Count; i++) {
                        _dataIndex = i;
                    }
                }

//                _entityManager = World.Active.EntityManager;
                _imageReader = GetComponent<ImageReader>();
                _imageValues = _imageReader.ProcessImage(activeImage);
            }

        private void Update()
            {
                Session.GetTrackables<AugmentedImage>(_augmentedImages, TrackableQueryFilter.Updated);

                foreach (var image in _augmentedImages) {
                    if (image.DatabaseIndex == _dataIndex) {
                        Debug.Log("passed index phase");

                        if (image.TrackingState == TrackingState.Tracking) {
                            if (!_created) {
                                Debug.Log("creating image");
                                instance = CreateImage(activeImage, image.ExtentX, image.ExtentZ);
                                _created = true;
                            }
                            instance.transform.localPosition = image.CenterPose.position;
                        } else {
                            if (_created) {
                                DestroyImmediate(instance);
                            }
                        }
                    }
                }
            }

    }
}