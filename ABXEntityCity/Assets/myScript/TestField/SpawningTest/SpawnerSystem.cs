﻿using myScript.TestField.LifeTest;
using myScript.TestField.ScaleTest;
using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Random = Unity.Mathematics.Random;

namespace myScript.TestField.SpawningTest
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class SpawnerSystem : JobComponentSystem
    {
        private BeginInitializationEntityCommandBufferSystem _beginCommandBuffer;

        protected override void OnCreate()
        {
            _beginCommandBuffer = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
        }

        struct SpawnJob : IJobForEachWithEntity<SpawnerComp, LocalToWorld>
        {
            public EntityCommandBuffer.Concurrent CommandBuffer;

            [BurstCompile]
            public void Execute(Entity entity, int index, ref SpawnerComp spawner, ref LocalToWorld location)
            {
                var random = new Random(1);
                for (int x = 0; x < spawner.CountX; x++)
                {
                    for (int y = 0; y < spawner.CountY; y++)
                    {
                        Entity instance = CommandBuffer.Instantiate(index, spawner.Prefab);

                        //create the position of the object
                        var position = math.transform(location.Value,
                            new float3(x * 1.3f, noise.cnoise(new float2(x, y) * 0.21f) * 2, y * 1.3f));

                        CommandBuffer.SetComponent(index, instance,
                            new Translation {Value = position});
                        CommandBuffer.SetComponent(index, instance,
                            new LifeTimeComp {Value = random.NextFloat(10.0f, 100.0f)});
                        CommandBuffer.SetComponent(index, instance,
                            new ScaleSpeed_Comp {ScalePerSecond = random.NextFloat(0.0f, 3.0f)});
                    }
                }

                CommandBuffer.DestroyEntity(index, entity);
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new SpawnJob
            {
                CommandBuffer = _beginCommandBuffer.CreateCommandBuffer().ToConcurrent()
            }.Schedule(this, inputDeps);
            _beginCommandBuffer.AddJobHandleForProducer(job);
         
            return job;
        }
    }
}