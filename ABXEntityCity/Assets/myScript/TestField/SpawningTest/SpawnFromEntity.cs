//using Unity.Entities;
//using Unity.Jobs;
//using Unity.Collections;
//using Unity.Mathematics;
//using Unity.Transforms;
//
//namespace myScript.TestField.SpawningTest {
//    /// JobComponentSystems can run on worker threads
//    /// Removing and creating entities can only be done on the main thread
//    /// This prevents race conditions
//    /// The system uses an EntityCommandBuffer to tell what tasks to do inside the job
//    /// SimulationSystemGroup runs on FixedUpdate
//    /// is intended to help the iterative simulation work become repeatable given identical inputs
//    /// the simulation group seems to be a means for layering what is called at certain locations of Update
//    /// https://forum.unity.com/threads/why-is-simulation-the-default-system-group.639058/
//    [UpdateInGroup(typeof(SimulationSystemGroup))]
//    public class SpawnFromEntity : JobComponentSystem {
//
//        private BeginInitializationEntityCommandBufferSystem _commandBufferSystem;
//
//        protected override void OnCreate()
//            {
//                // called on create so to cache buffer system once
//                _commandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
//            }
//
//
//        struct SpawnJob : IJobForEachWithEntity<SpawnerComp, LocalToWorld> {
//
//            public EntityCommandBuffer.Concurrent CommandBuffer;
//
//            public void Execute(Entity entity, int index,
//                [ReadOnly] ref SpawnerComp spawner,
//                [ReadOnly] ref LocalToWorld location)
//                {
//                    for (int x = 0; x < spawner.CountX; x++) {
//                        for (int y = 0; y < spawner.CountY; y++) {
//                            //spawn the prefab called from conversion ans assigned to spawner entity
//                            var instance = CommandBuffer.Instantiate(index, spawner.Prefab);
//
//                            //create the position of the object
//                            var position = math.transform(location.Value,
//                                new float3(x * 1.3f, noise.cnoise(new float2(x, y) * 0.21f) * 2, y * 1.3f));
//
//                            //set the position 
//                            CommandBuffer.SetComponent(index, instance, new Translation {Value = position});
//                        }
//                    }
//
//                    //destroy reference 
//                    CommandBuffer.DestroyEntity(index, entity);
//                }
//        }
//
//
//        protected override JobHandle OnUpdate(JobHandle inputDeps)
//            {
//                var job = new SpawnJob {
//                    CommandBuffer = _commandBufferSystem.CreateCommandBuffer().ToConcurrent()
//                }.Schedule(this, inputDeps);
//
//                // SpawnJob runs in parallel with no sync point until barrier system executes
//                // When the barrier system executes we complete SpawnJob and then play back the commands
//                // (Creating and placing the entity)
//                // This tells the barrier system which job to complete before it can play back the commands
//                _commandBufferSystem.AddJobHandleForProducer(job);
//                return job;
//            }
//
//    }
//}