using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace myScript.TestField.SpawningTest {
    public class SpawnFromMono : MonoBehaviour {

        public GameObject Prefab;
        public int CountX = 100;
        public int CountY = 100;

        private void Start()
            {
                //only create entity once for spawning 
                var prefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(Prefab, World.Active);
                var entityManager = World.Active.EntityManager;

                for (int x = 0; x < CountX; x++) {
                    for (int y = 0; y < CountY; y++) {
                        //spawn converted object as an entity 
                        var instance = entityManager.Instantiate(prefab);

                        //create a position with a bit of noise 
                        //TransformPoint sets local space to world space
                        var position = transform.TransformPoint(new float3(x * 1.3f,
                            noise.cnoise(new float2(x, y) * 0.21f) * 2, y * 1.3f));

                        //position the entity by setting translation
                        entityManager.SetComponentData(instance, new Translation {Value = position});
                    }
                }
            }

    }
}