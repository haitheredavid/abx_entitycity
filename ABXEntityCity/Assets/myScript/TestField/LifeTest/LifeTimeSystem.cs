using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using UnityEngine;

namespace myScript.TestField.LifeTest {
    public struct LifeTimeComp : IComponentData {

        public float Value;

    }

    public class LifeTimeSystem : JobComponentSystem {

        private EntityCommandBufferSystem _commandBuffer;

        protected override void OnCreate()
            {
                _commandBuffer = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
            }


        [BurstCompile]
        private struct LifeTimeJob : IJobForEachWithEntity<LifeTimeComp> {

            public float DeltaTime;

            [WriteOnly] public EntityCommandBuffer.Concurrent CommandBuffer;

            public void Execute(Entity entity, int index, ref LifeTimeComp lifetime)
                {
                    lifetime.Value -= DeltaTime;

                    if (lifetime.Value < 0.0f) {
                        CommandBuffer.DestroyEntity(index, entity);
                    }
                }

        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
            {
                var commandBuffer = _commandBuffer.CreateCommandBuffer().ToConcurrent();

                var job = new LifeTimeJob {
                    DeltaTime = Time.deltaTime,
                    CommandBuffer = commandBuffer
                }.Schedule(this, inputDeps);

                _commandBuffer.AddJobHandleForProducer(job);
                return job;
            }

    }
}