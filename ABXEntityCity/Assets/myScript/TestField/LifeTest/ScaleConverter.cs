﻿using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace myScript.TestField.LifeTest
{
    [RequiresEntityConversion]
    public class ScaleConverter : MonoBehaviour, IConvertGameObjectToEntity
    {
        public float ScaleSpeed = 2.0f;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponentData(entity, 
                new Scale {Value = gameObject.transform.localScale.x});
            dstManager.AddComponentData(entity, 
                new ScaleSpeedComp {ScalePerSecond = ScaleSpeed});
            dstManager.AddComponentData(entity,
                new LifeTimeComp{ Value = 0.0f}); 
        }
    }
}