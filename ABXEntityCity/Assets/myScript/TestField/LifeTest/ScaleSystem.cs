﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace myScript.TestField.LifeTest {
    public class ScaleSystem : JobComponentSystem {

        private EntityQuery _group;

        protected override void OnCreate()
            {
                _group = GetEntityQuery(typeof(Scale), ComponentType.ReadOnly<ScaleSpeedComp>());
            }


        [BurstCompile]
        struct ScaleChunkSpeedJob : IJobChunk {

            public float time;
            public ArchetypeChunkComponentType<Scale> ScaleType;
            [ReadOnly] public ArchetypeChunkComponentType<ScaleSpeedComp> ScaleSpeedType;

            public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
                {
                    var scaleChunk = chunk.GetNativeArray(ScaleType);
                    var scaleSpeedChunk = chunk.GetNativeArray(ScaleSpeedType);

                    for (int i = 0; i < chunk.Count; i++) {
                        var scale = scaleChunk[i];
                        var scaleSpeed = scaleSpeedChunk[i];

                        scaleChunk[i] = new Scale {Value = math.sin(time * scaleSpeed.ScalePerSecond)};
                    }
                }

        }


        protected override JobHandle OnUpdate(JobHandle inputDeps)
            {
                var scaleType = GetArchetypeChunkComponentType<Scale>();
                var scaleSpeedType = GetArchetypeChunkComponentType<ScaleSpeedComp>(true);

                var job = new ScaleChunkSpeedJob {
                    ScaleType = scaleType,
                    ScaleSpeedType = scaleSpeedType,
                    time = Time.time
                };

                return job.Schedule(_group, inputDeps);
            }

    }
}