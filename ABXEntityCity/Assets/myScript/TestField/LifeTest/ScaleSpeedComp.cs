﻿using System;
using Unity.Entities;

namespace myScript.TestField.LifeTest{
    [Serializable]
    public struct ScaleSpeedComp : IComponentData {

        public float ScalePerSecond;

    }
}