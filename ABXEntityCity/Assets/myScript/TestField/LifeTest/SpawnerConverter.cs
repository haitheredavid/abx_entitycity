using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace myScript.TestField.LifeTest {
    [RequiresEntityConversion]
    public class SpawnerConverter : MonoBehaviour, IDeclareReferencedPrefabs, IConvertGameObjectToEntity {

        public GameObject Prefab;
        public int CountX;
        public int CountY;

        //referenced prefabs need to be declared so the conversion system knows about it head of time
        public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
            {
                referencedPrefabs.Add(Prefab);
            }

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
            {
                //create an entity that will act as the spawner of the 0object 
                //it will referenced the converted prefab from before. 
                var spawnerData = new SpawnerComp {
                    Prefab = conversionSystem.GetPrimaryEntity(Prefab),
                    CountX = CountX,
                    CountY = CountY
                };

                dstManager.AddComponentData(entity, spawnerData);
            }

    }
}