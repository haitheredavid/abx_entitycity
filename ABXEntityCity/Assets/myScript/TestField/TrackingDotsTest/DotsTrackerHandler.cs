﻿using System;
using System.Collections.Generic;
using GoogleARCore;
using myScript.TestField.VisualizeImageTest;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace myScript.TestField.TrackingDotsTest {
    public class DotsTrackerHandler : MonoBehaviour {

        public Color _lostColor = new Color(50f, 50f, 50f, 255f);
        public Color _pausedColor = new Color(0f, 0f, 255f, 255f);
        public Color _trackingColor = new Color(0f, 255f, 0f, 255f);
        public Color _nothingColor = new Color(0f, 255f, 0f, 255f);

        public AugmentedImageDatabase database;

        public Image panel;
        public TextMeshProUGUI trackingText;

        private Color _stateColor;
        private List<AugmentedImage> _augmentedImages = new List<AugmentedImage>();

        private bool _createdEntity = false;
        private int _dataIndex;

        private void Awake()
            {
                if (database != null) {
                    for (int i = 0; i < database.Count; i++) {
                        _dataIndex = i;
                    }
                }
            }

        private void UpdateTrackerUI()
            {
                switch (Session.Status) {
                    case SessionStatus.Tracking:
                        _stateColor = _trackingColor;
                        break;
                    case SessionStatus.LostTracking:
                        _stateColor = _lostColor;
                        break;
                    case SessionStatus.None:
                        _stateColor = _nothingColor;
                        break;
                    case SessionStatus.NotTracking:
                        _stateColor = _pausedColor;
                        break;
                }

                trackingText.text = Session.Status.ToString();
                trackingText.color = _stateColor;
            }

        private void Update()
            {
                UpdateTrackerUI();

                if (Session.Status != SessionStatus.Tracking) {
                    Screen.sleepTimeout = SleepTimeout.SystemSetting;
                } else {
                    Screen.sleepTimeout = SleepTimeout.NeverSleep;
                }

                Session.GetTrackables<AugmentedImage>(_augmentedImages, TrackableQueryFilter.Updated);

                foreach (var image in _augmentedImages) {
                    if (image.DatabaseIndex == _dataIndex) {
                        Debug.Log("passed index phase");
                        if (image.TrackingState == TrackingState.Tracking) {
                            
                            if (!_createdEntity) {
                                Debug.Log("create entity");
                                ValueManager.GetInstance().CreateEntity(image.CenterPose.position);
                                _createdEntity = true;
                            }

                            panel.color = _stateColor;
                            Debug.Log("Moving Objects");
                            
                            
                        } else if (image.TrackingState == TrackingState.Stopped) {
                            if (_createdEntity != null) {
                                Debug.Log("Getting rid of entity");
                                _createdEntity = false;
                            }
                            panel.color = _stateColor;
                        }
                    }
                }
            }

    }
}