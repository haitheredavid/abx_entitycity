using Unity.Mathematics;
using UnityEngine;

namespace myScript.TestField.VisualizeImageTest {
    public class ImageReader : MonoBehaviour {
        
        // script created for reading an image at runtime 
        public float3[] ProcessImage(Texture2D image)
            {
                var values = ReadImage(image);
                return values;
            }

        private static int GetPixelCount(Texture2D source)
            {
                return source.width * source.height;
            }

        private static float3[] ReadImage(Texture2D source)
            {
                Color[] _pixels = source.GetPixels(0, 0, source.width, source.height);
                float3[] tempArray = new float3[_pixels.Length];

                for (int i = 0, x = 0; x < source.width; x++) {
                    for (int z = 0; z < source.height; z++, i++) {
                        tempArray[i] = new float3(x, _pixels[i].grayscale, z);
                    }
                }
                Debug.Log("total image count is " +  tempArray.Length);
                return tempArray;
            }
    }
}