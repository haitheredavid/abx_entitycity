using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

namespace myScript.TestField.VisualizeImageTest {
    public struct Tile : IComponentData {

        public float PixelWidth;
        public float PixelHeight;

    }

    public struct Pixel : IComponentData {

        public float3 Start;
        public float3 Value;

    }

    public struct SetTileData : IComponentData { }

    [UpdateInGroup(typeof(TransformSystemGroup))]
    public class TileSystemJob : JobComponentSystem {

        [BurstCompile]
        struct TileSystemBurstJob :  IJobForEachWithEntity<Translation, Pixel, NonUniformScale> {

            public float Time;

            public void Execute(Entity entity, int index,
                ref Translation translation,
                ref Pixel pixel,
                ref NonUniformScale scale)
                {
                    scale.Value = math.lerp(new float3(1f, 1f, 1f), new float3(1f, 1f * pixel.Value.y, 1f),
                        math.sin(Time));
                    translation.Value = new float3(pixel.Start.x, scale.Value.y / 2, pixel.Start.z);
                }

        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
            {
                var job = new TileSystemBurstJob {Time = Time.time};
                return job.Schedule(this, inputDeps);
            }

    }

//
//    [UpdateInGroup(typeof(SimulationSystemGroup))]
//    public class CreateTileSystem : JobComponentSystem {
//
//        private static EntityArchetype _pixelType;
//        private BeginInitializationEntityCommandBufferSystem _barrier;
//
//        protected override void OnCreate()
//            {
//                _barrier = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
//
//                _pixelType = EntityManager.CreateArchetype(
//                    typeof(Translation),
//                    typeof(RenderMesh),
//                    typeof(LocalToWorld),
//                    typeof(NonUniformScale),
//                    typeof(Pixel)
//                );
//            }
//
//        struct CreateTileSystemJob : IJobForEachWithEntity<Tile, SetTileData> {
//
//            // array to send the values in 
//            [DeallocateOnJobCompletion] public NativeArray<float3> ImageValues;
//
//            // buffer to set entity content 
//            public EntityCommandBuffer.Concurrent CommandBuffer;
//
//            // 
//            public Entity PixelType;
//
//            public void Execute(Entity entity, int index, ref Tile tile, ref SetTileData tileData)
//                {
//                    for (int i = 0; i < ImageValues.Length; i++) {
//                        var instance = CommandBuffer.Instantiate(index, PixelType);
//
//                        CommandBuffer.SetComponent(index, instance, new Pixel {
//                            Value =  new float3(ImageValues[i].x, ImageValues[i].y * 10f, ImageValues[i].z),
//                            Start = new float3(ImageValues[i].x, 0f, ImageValues[i].z)
//                        });
//
//                        CommandBuffer.SetComponent(index, instance, new Translation {
//                            Value =  new float3(ImageValues[i].x, 0f, ImageValues[i].z)
//                        });
//                    }
//
//                    CommandBuffer.RemoveComponent(index, entity, typeof(SetTileData));
//                    ImageValues.Dispose();
//                }
//
//        }
//
//        protected override JobHandle OnUpdate(JobHandle inputDeps)
//            {
//          
//
//                var tempEnt = EntityManager.CreateEntity(
//                    typeof(Tile),
//                    typeof(SetTileData)
//                );
//
//                EntityManager.SetName(tempEnt, "Tile");
//                EntityManager.SetComponentData(tempEnt, new SetTileData {
//                    mesh = ValueManager.GetInstance().mesh,
//                    material = ValueManager.GetInstance().material
//                });
//
//         
//                var values = ValueManager.GetInstance().GetImageValues();
//                NativeArray<float3> tempValues = new NativeArray<float3>(values.Length, Allocator.TempJob);
//
//                for (int i = 0; i < values.Length; i++) {
//                    tempValues[i] = values[i];
//                }
//
//                var entity = _barrier.EntityManager.CreateEntity(_pixelType);
//
//                var job = new CreateTileSystemJob {
//                    CommandBuffer = _barrier.CreateCommandBuffer().ToConcurrent(),
//                    ImageValues = tempValues,
//                    PixelType = entity
//                }.Schedule(this, inputDeps);
//
//                job.Complete();
//
//                _barrier.EntityManager.DestroyEntity(entity);
//                tempValues.Dispose();
//
//                return job;
//            }
//
//    }
//}

    public class TileSystem : ComponentSystem {

        private float3[] _values;

        private EntityArchetype _pixelType;

        protected override void OnCreate()
            {
                _pixelType = EntityManager.CreateArchetype(
                    typeof(Translation),
                    typeof(RenderMesh),
                    typeof(LocalToWorld),
                    typeof(NonUniformScale),
                    typeof(Pixel)
                );
            }

        protected override void OnUpdate()
            {
                Entities.ForEach((Entity id,
                    ref Tile tile,
                    ref Translation translation,
                    ref SetTileData setTile) => {
                    Entity entity = EntityManager.CreateEntity(_pixelType);

                    _values = ValueManager.GetInstance().GetImageValues();
                    for (int i = 0; i < _values.Length; i++) {
                        var instance = EntityManager.Instantiate(entity);

                        var end = new float3(_values[i].x, _values[i].y * 10f, _values[i].z) + translation.Value;
                        var start  = new float3(_values[i].x, 0f, _values[i].z) + translation.Value;

                        EntityManager.SetSharedComponentData(instance, new RenderMesh {
                            mesh = ValueManager.GetInstance().mesh,
                            material = ValueManager.GetInstance().material
                        });

                        EntityManager.SetComponentData(instance, new Pixel {
                            Value =  end,
                            Start = start
                        });
                        EntityManager.SetComponentData(instance, new Translation {
                            Value = start
                        });
                    }
                    EntityManager.DestroyEntity(entity);
                    EntityManager.RemoveComponent(id, typeof(SetTileData));
                });

                Entities.ForEach((ref Translation translation, ref NonUniformScale scale, ref Pixel pixel) => {
                    var startScale =  new float3(1f, 1f, 1f);
                    var endScale = new float3(1f, 1f * pixel.Value.y, 1f);
                    scale.Value = math.lerp(startScale, endScale, math.sin(Time.time ));
                    translation.Value = new float3(pixel.Start.x, scale.Value.y / 2, pixel.Start.z);
                });
            }

    }
}