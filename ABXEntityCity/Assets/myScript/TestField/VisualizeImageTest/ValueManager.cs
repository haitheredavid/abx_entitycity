using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace myScript.TestField.VisualizeImageTest {
    public class ValueManager : MonoBehaviour {

        public Mesh mesh;
        public Material material;
        public Texture2D activeImage;

        private static EntityManager _entityManager;
        private static EntityArchetype _createPixelType;
        private static EntityArchetype _tileType;

        private ImageReader _imageReader;
        private float3[] _imageValues;

    

        public float3[] GetImageValues()
            {
                return _imageValues;
            }

        private static ValueManager Instance;

        public static ValueManager GetInstance()
            {
                return Instance;
            }

        private void Awake()
            {
                Instance = this;

                _entityManager = World.Active.EntityManager;
                _imageReader = GetComponent<ImageReader>();
                _imageValues = _imageReader.ProcessImage(activeImage);
                
                
            }

        public void CreateEntity(float3 location)
            {
                _tileType = _entityManager.CreateArchetype(
                    typeof(Tile),
                    typeof(SetTileData),
                    typeof(Translation)
                );

                var temp = _entityManager.CreateEntity(_tileType);
                _entityManager.SetComponentData(temp, new Translation {Value = location});
            }

    }
}